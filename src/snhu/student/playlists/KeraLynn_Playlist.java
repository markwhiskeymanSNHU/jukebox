package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class KeraLynn_Playlist  {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> eltonJohnTracks = new ArrayList<Song>();
    EltonJohn eltonJohnBand = new EltonJohn();
	
    eltonJohnTracks = eltonJohnBand.getEltonJohnSongs();
	
	playlist.add(eltonJohnTracks.get(0));
	playlist.add(eltonJohnTracks.get(1));
	playlist.add(eltonJohnTracks.get(2));
	
	
    ThirdEyeBlind thirdEyeBlindBand = new ThirdEyeBlind();
	ArrayList<Song> thirdEyeBlindTracks = new ArrayList<Song>();
	thirdEyeBlindTracks = thirdEyeBlindBand.getThirdEyeBlindSongs();
	
	playlist.add(thirdEyeBlindTracks.get(0));
	playlist.add(thirdEyeBlindTracks.get(1));
	playlist.add(thirdEyeBlindTracks.get(2));
	
    return playlist;
	}
}