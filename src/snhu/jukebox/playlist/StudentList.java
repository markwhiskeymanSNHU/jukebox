package snhu.jukebox.playlist;

import snhu.student.playlists.*;

import java.util.ArrayList;
import java.util.List;

public class StudentList {

	public StudentList(){
	}

	public List<String> getStudentsNames() {
		ArrayList<String> studentNames = new ArrayList<String>();

		String StudentName1 = "TestStudent1Name";
		studentNames.add(StudentName1);

		String StudentName2 = "TestStudent2Name";
		studentNames.add(StudentName2);

		//Module 5 Code Assignment
		//Add your name to create a new student profile
		//Use template below and put your name in the areas of 'StudentName'
		String RachelSickler = "Rachel Sickler";
		studentNames.add(RachelSickler);

		String RyleeThompson = "Rylee Thompson";
		studentNames.add(RyleeThompson);

		String JimLouis = "Jim Louis";
		studentNames.add(JimLouis);

		String TravisOwens = "Travis Owens";
		studentNames.add(TravisOwens);
		
		String FelixTudoran = "Felix Tudoran";
		studentNames.add(FelixTudoran);
		
		String DanGuerin = "Dan Guerin";
		studentNames.add(DanGuerin);
		
		String AlexHamilton = "Alex Hamilton";
		studentNames.add(AlexHamilton);
		
		String TylerBeckham = "Tyler Beckham";
		studentNames.add(TylerBeckham);
		
		String KeraLynn = "Kera Lynn";
		studentNames.add(KeraLynn);

		
		return studentNames;
	}

	public Student GetStudentProfile(String student){
		Student emptyStudent = null;

		switch(student) {
		   case "TestStudent1_Playlist":
			   TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();
			   Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());
			   return TestStudent1;

		   case "TestStudent2_Playlist":
			   TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
			   Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
			   return TestStudent2;
			   
		   case "KeraLynn_Playlist":
			   KeraLynn_Playlist keraLynnPlaylist = new KeraLynn_Playlist();
			   Student KeraLynn = new Student("Kera Lynn", keraLynnPlaylist.StudentPlaylist());
			   return KeraLynn;
			   
	       // Get Rylee Thompson student profile
		   case "RyleeThompson_Playlist":
		       RyleeThompson_Playlist ryleeThompsonPlaylist = new RyleeThompson_Playlist();
			   Student RyleeThompson = new Student("Rylee Thompson", ryleeThompsonPlaylist.StudentPlaylist());
			   return RyleeThompson;
			
		   case "TravisOwens_Playlist":
			   TravisOwens_Playlist travisOwensPlaylist = new TravisOwens_Playlist();
			   Student TravisOwens = new Student("TravisOwens", travisOwensPlaylist.StudentPlaylist());
			   return TravisOwens;
		   //Module 6 Code Assignment - Add your own case statement for your profile. Use the above case statements as a template.

		}
		return emptyStudent;
	}
}
