/*
 * Author: Alex Hamilton
 * 
 * Stereolab are an English-French avant-pop band formed in London in 1990
 */
package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Stereolab {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Stereolab() {
    }
    
    //Songs from 1994 album "Mars Audiac Quintet"
    public ArrayList<Song> getStereolabSongs() {
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Three-Dee Melodie", "Stereolab");             	//Create a song
         Song track2 = new Song("Ping Pong", "Stereolab");         				//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Stereolab
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Stereolab 
         return albumTracks;                                                    //Return the songs for the Stereolab in the form of an ArrayList
    }
}